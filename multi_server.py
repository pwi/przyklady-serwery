#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, time, katechizm
from multiprocessing import Process
from simple_server import server_loop
from threading import Thread

WORKER_CLASSES = {'thread': Thread, 'process': Process}
WORKER_MAX = 10

def start_worker(Worker, listen_sock):
    worker = Worker(target=server_loop, args=(listen_sock,))
    worker.daemon = True # zakończ, gdy główny proces się zkończy
    worker.start()
    return worker


if __name__ == '__main__':

    if len(sys.argv) != 3 or sys.argv[2] not in WORKER_CLASSES:
        print >>sys.stderr, 'usage: %s adres thread|process' % sys.argv[0]
        sys.exit(2)

    Worker = WORKER_CLASSES[sys.argv.pop()] # setup() potrzebuje len(argv)==2
    listen_sock = katechizm.setup()
    workers = []
    for i in range(WORKER_MAX):
        workers.append(start_worker(Worker, listen_sock))
    # Sprawdzanie co 2 sekundy martwych robotników i ich zamiana
    while True:
        time.sleep(2)
        for worker in workers:
            if not worker.is_alive():
                print worker.name, "umarł; zaczynam reinkarnację"
                workers.remove(worker)
                workers.append(start_worker(Worker, listen_sock))
