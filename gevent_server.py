#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gevent.monkey
gevent.monkey.patch_socket()

import katechizm
from simple_server import handle_client


def server_loop(listen_sock):
    while True:
        client_sock, sockname = listen_sock.accept()
        print 'accept on %d' % sockname[1]
        gevent.spawn(handle_client, client_sock)


if __name__ == '__main__':
    listen_sock = katechizm.setup()
    server_loop(listen_sock)
