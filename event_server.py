#!/usr/bin/env python
#-*- coding: utf-8 -*-

import select, katechizm


def accept(sock):
    """
        Akceptuje nowe połączenia
    """
    newsock, sockname = sock.accept()
    # Ustawienie odczytu jako nieblokującego
    newsock.setblocking(False)
    fd = newsock.fileno()
    sockets[fd] = newsock
    poll.register(fd, select.POLLIN)
    requests[newsock] = ''


def clean(sock, fd):
    """
        Usuwa zamknięte połączenia z listy
    """
    poll.unregister(fd)
    del sockets[fd]
    requests.pop(sock, None)
    responses.pop(sock, None)


def handle_request(sock):
    """
        Kolekcjonuje przychodzące dane do czasu sformułowania pytania
    """
    data = sock.recv(4096)
    if not data: # koniec danych
        sock.close() # wygeneruje zdarzenie POLLNVAL
        return
    requests[sock] += data
    if katechizm.ends_with(requests[sock], ['?']):
        question = requests.pop(sock)
        answer = katechizm.qadict[question]
        poll.modify(sock, select.POLLOUT)
        responses[sock] = answer


def handle_response(sock):
    """
        Wysyła fragmenty każdej odpowiedzi, dopóki wszystkie nie są wysłane
    """
    response = responses.pop(sock)
    n = sock.send(response)
    if n < len(response):
        responses[sock] = response[n:]
    else:
        poll.modify(sock, select.POLLIN)
        requests[sock] = ''


def server_loop(listen_sock):
    while True:
        for fd, event in poll.poll():
            sock = sockets[fd]
            if sock is listen_sock:
                accept(sock)
            elif event & (select.POLLHUP | select.POLLERR | select.POLLNVAL):
                clean(sock, fd)
            elif event & select.POLLIN:
                handle_request(sock)
            elif event & select.POLLOUT:
                handle_response(sock)


if __name__ == '__main__':
    listen_sock = katechizm.setup()
    sockets = { listen_sock.fileno(): listen_sock }
    requests = {}
    responses = {}
    poll = select.poll()
    poll.register(listen_sock, select.POLLIN)
    server_loop(listen_sock)