#!/usr/bin/env python
# -*- coding: utf-8 -*-

import katechizm

def handle_client(client_sock):
    try:
        while True:
            question = katechizm.recv_until(client_sock, ['?'])
            answer = katechizm.qadict[question]
            client_sock.sendall(answer)
    except EOFError:
        client_sock.close()


def server_loop(listen_sock):
    while True:
        client_sock, sockname = listen_sock.accept()
        print 'accept on %d' % sockname[1]
        handle_client(client_sock)


if __name__ == '__main__':
    listen_sock = katechizm.setup()
    server_loop(listen_sock)
