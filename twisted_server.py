#!/usr/bin/env python
# -*- coding: utf-8 -*-

from twisted.internet.protocol import Protocol, ServerFactory
from twisted.internet import reactor
import katechizm

class Katechizm(Protocol):

    def connectionMade(self):
        self.question = ''

    def dataReceived(self, data):
        self.question += data
        if katechizm.ends_with(self.question, ['?']):
            self.transport.write(katechizm.qadict[self.question])
            self.question = ''


if __name__ == '__main__':
    factory = ServerFactory()
    factory.protocol = Katechizm
    reactor.listenTCP(4444, factory)
    reactor.run()