#!/usr/bin/env python
# -*- coding: utf-8 -*-

from SocketServer import ThreadingMixIn, TCPServer, BaseRequestHandler
import katechizm, simple_server, socket


class MyHandler(BaseRequestHandler):

    def handle(self):
        simple_server.handle_client(self.request)


class MyServer(ThreadingMixIn, TCPServer):
    allow_reuse_address = 1


if __name__ == '__main__':
    server = MyServer(('', katechizm.PORT), MyHandler)
    server.serve_forever()
