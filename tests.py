#!/usr/bin/env python
# -*- coding: utf-8 -*-

from funkload.FunkLoadTestCase import FunkLoadTestCase
import socket
import os
import unittest
import logging

import katechizm


SERVER_HOST = os.environ.get('KATECHIZM_SERVER', 'localhost')
SERVER_PORT = int(os.environ.get('KATECHIZM_PORT', katechizm.PORT))


class TestKatechizm(FunkLoadTestCase):
    def test_dialog(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((SERVER_HOST, SERVER_PORT))
        for qa in katechizm.qa:
            question, answer = qa
            sock.sendall(question)
            reply = katechizm.recv_until(sock, ['.', '!'])
            self.assertEqual(reply, answer)
        sock.close()


if __name__ == '__main__':
    unittest.main()
