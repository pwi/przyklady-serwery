#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket, sys, katechizm

def client(hostname, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((hostname, port))
    answers = []
    for row in katechizm.qa:
        sock.sendall(row[0])
        answer = katechizm.recv_until(sock, ['.', '!'])
        answers.append(answer)
    for answer in answers:
        print answer


if __name__ == '__main__':
    if not 2 <= len(sys.argv) <= 3:
        print >>sys.stderr, 'użycie: %s address [port]' % sys.argv[0]
        sys.exit(2)
    port = int(sys.argv[2]) if len(sys.argv) > 2 else katechizm.PORT
    client(sys.argv[1], port)
