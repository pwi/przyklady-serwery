#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import time
from daemon import runner

class App:
    def __init__(self):
        self.stdin_path = '/dev/null'
        self.stdout_path = '/dev/tty'
        self.stderr_path = '/dev/tty'
        self.pidfile_path = '/tmp/demon.pid'
        self.pidfile_timeout = 5

    # Główny kod aplikacji
    def run(self):
        while True:
            logger.info("Info message")
            time.sleep(10)


# Ustawienie loggera
logger = logging.getLogger("DemonLog")
logger.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler = logging.FileHandler("/tmp/demon.log")
handler.setFormatter(formatter)
logger.addHandler(handler)

# Stworzenie i uruchomienie aplikacji jako demona
app = App()
daemon_runner = runner.DaemonRunner(app)
# Zapobiegnięcie zamknięciu pliku logów podczas demonizacji
daemon_runner.daemon_context.files_preserve = [handler.stream]
daemon_runner.do_action()
