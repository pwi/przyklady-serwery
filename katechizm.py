#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket, sys

PORT = 4444
# za http://pl.wikisource.org/wiki/Katechizm_polskiego_dziecka
qa = (
    ('Kto ty jesteś?', 'Polak mały.'),
    ('Jaki znak twój?', 'Orzeł biały.'),
    ('Gdzie ty mieszkasz?', 'Między swemi.'),
    ('W jakim kraju?', 'W polskiej ziemi.'),
    ('Czem ta ziemia?', 'Mą Ojczyzną.'),
    ('Czem zdobyta?', 'Krwią i blizną.'),
    ('Czy ją kochasz?', 'Kocham szczerze.'),
    ('A w co wierzysz?', 'W Polskę wierzę!'),
    ('Coś ty dla niej?', 'Wdzięczne dziécię.'),
    ('Coś jej wininen?', 'Oddać życie.')
)

qadict = dict(qa)


def ends_with(message, suffixes):
    for suffix in suffixes:
        if message.endswith(suffix):
            return True
    return False


def recv_until(sock, suffixes):
    message = ''
    while not ends_with(message, suffixes):
        data = sock.recv(4096)
        if not data:
            raise EOFError('Gniazdo zamknięte przed otrzymaniem jednego z %r możliwych zakończeń' % ','.join(suffixes))
        message += data
    return message


def setup():
    if len(sys.argv) != 2:
        print >>sys.stderr, 'użycie: %s adres' % sys.argv[0]
        exit(2)
    address = sys.argv[1]
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind((address, PORT))
    sock.listen(128)
    print 'Gotowy do nasłuchiwania %r na porcie %d' % (address, PORT)
    return sock
